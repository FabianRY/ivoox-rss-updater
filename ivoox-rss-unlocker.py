#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Sat Jan  7 09:13:30 2023

@author: Fabian Robledo
@mail: frobledo@gmx.com

Given an ivoox rss feed, download the audio files and
creates an rss that you can import in Antennapod once 
you put the new rss in your server.

I have tested ivoox servers and it seems that the links
to the audio file are dinamic: They change after a time. 
More or less about 30 minutes, which is a hassle for long 
podcasts. The current approach seeks to download the file 
to keep it in your own server.
"""

# Standard library
import argparse
import logging
import os
import os.path
import time
import urllib


# Selenium imports to connect and browse ivoox
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

import xml.etree.ElementTree as ET


logging.basicConfig(level=logging.INFO)

options  = Options()
options.add_argument("--headless")
FIREFOX = webdriver.Firefox(options=options)
DOWNLOAD_BUTTON_ID:str="dlink"
SAVE_FOLDER:str="/tmp/ivoox/"

def parseargs():
    """
        Parses needed arguments

        Right now only the rss link is need, and an 
        outfile for the new rss.

        TODO: Path to store the sound files
        TODO: URL for the new server
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--rss", required=True)
    parser.add_argument("-outfile", "-o", type=str, required=True)
    parser.add_argument("--headless", "-hl", type=bool,default=False)
    parser.add_argument("--server", "-s", type=str, required=False)
    parser.add_argument("--ivoox", "-ivoox", action='store_true', default=True)
    parser.add_argument("--save", "-sd", type=str, required=False)
    return parser.parse_args()

def connect_to_RSS(URL) -> str:
    """
        checks whether the URL is available

        If it is not, then exits the program

        TODO handle errors
    """
    data = urllib.request.urlopen(URL)
    if data.status == 404:
        logging.error("RSS not found")
    elif data.status == 500:
        logging.error("Server error")
    return data.read().decode('utf-8')

def filter_rss_audio_links(RSS) -> list:
    """
        Given the full RSS content, gets the audio links 
    """
    links = list()
    for i in range(14,len(RSS[0])):
        # RSS[0] is the channel element, that
        # includes everything. In that node
        # Elements 14 onwards are episodes
        # On each, element in position 1 is the link
        links.append(RSS[0][i][1].text)
    return links

def get_audio_from_link(LINK:str, save_folder:str = "") -> str:
    logging.debug(LINK)
    try:
        # Tries to avoid the cookies popup
        cookies = FIREFOX.find_element(By.ID, "didomi-notice-agree-button")
        cookies.click()
    except NoSuchElementException:
        logging.debug("No cookies found")
        pass
    while True:
        try:
            FIREFOX.get(LINK)
            FIREFOX.execute_script("document.getElementById('dlink').click();")
            break
        except Exception as e:
            logging.error("Could not load the ivoox page, retrying in 1...")
            time.sleep(1)
    # El codigo necesario para pulsar los botones en la interfaz de la forma
    # que un usuario debería hacer con la UI. Esto es legacy, tambien funcionaba
    # Pero resulta que se puede acceder directamente al enlace pinchando en el botón
    # con id dclick
    # try:
    #     FIREFOX.execute_script("downloadGroupingFunctions();")
    # except Exception:
    #     return print("Couldn't click download button")
    # download_button = FIREFOX.find_element(By.ID, "dlink")
    # FIREFOX.implicitly_wait(5)
    # FIREFOX.execute_script("arguments[0].style.visibility='visible'", download_button)
    # time.sleep(10)
    # download_button.click()
    while True:
         # Hay un recaptcha oculto al acceder. Muy ocasionalmente me 
         # ha causado errores. Tan ocasionalmente que reintarlo hace que funcione
         # Esto hay que vigilarlo
         try:
             link = FIREFOX.current_url
             break
         except Exception:
             logging.error("REcaptcha Error")
             time.sleep(1)
        
    time.sleep(1) # Espera 1 segundo. Debido al delay entre que se pulsa el botón y carga el audio
    # No esperar implicaría que utiliza la página del episodio, pero no el audio
    link = FIREFOX.current_url
    logging.debug(link)
    filename = link.split("/")[-1]
    if (save_folder != ""):
        logging.info("Downloading {}".format(filename))
        urllib.request.urlretrieve(link, SAVE_FOLDER+filename+".mp3")
    return link

def main():
    
    arguments = parseargs()
    rss_link:str = arguments.rss
    outfile:str = arguments.outfile
    server:str = arguments.server
    ivoox:bool = arguments.ivoox

    logging.info("RSS file: {}".format(rss_link))
    logging.info("Output file: {}".format(outfile))
    
    rss = connect_to_RSS(rss_link)
    ET_rss = ET.fromstring(rss)
    audio_links = filter_rss_audio_links(ET_rss)
    
    logging.info("Detected {} episodes".format(len(audio_links)))
    
    mp3_links = list(map(get_audio_from_link, audio_links))
    
    FIREFOX.implicitly_wait(5)
    FIREFOX.close()
    
    for i in range(14, 14+len(audio_links)):
        if mp3_links[i-14]:
            filename = mp3_links[i-14].split("/")[-1].split(".mp3")[0]
            if (ivoox):
                newlink = mp3_links[i-14]
            else:
                newlink = os.path.join(server, filename+".mp3")
            ET_rss[0][i][2].attrib["url"] = newlink
    
    logging.info("Saving rss")
    with open(outfile, "wb") as rss_hand:
        rss_hand.write(ET.tostring(ET_rss, encoding="utf-8"))
    pass

if __name__=="__main__":
    main()
