# IVOOX RSS updater

O actualizador del RSS de ivoox, hasta que vea un nombre que me
convenza más.

Este script modifica el rss de ivoox para que apunte directamente
al archivo de audio. En los podcast de ivoox originals, permite
escuchar los audios completos, en lugar de la vista previa. 
El RSS generado se puede usar en un gestor de podcast como antennapodd

# Requisitos

Python 3.10.12+
Selenium 4.7.2+

(Muy probablemente funcione en cualquier versión de python 3.5+
pero se ha testado en estas versiones)

Firefox instalado (Pendiente de hacerse compatible con chrome)

Un servidor donde alojar el RSS final


# Usos

## Primera intención

La intención original era utilizar el enlace directo a ivoox para
no descargar ningún archivo. Por desgracia, tiene pinta de que los
enlaces solo funcionan durante un tiempo tras encontrarlo. 

Para que estén siempre disponibles, un cron debe estar ejecutandose
contínuamuente para validar los enlaces. El tiempo es suficiente, 
por las pruebas que he hecho, para generar el RSS de un podcast 
de 50 episodios, alojarlo en el servidor, importarlo en antenapod y
descargar los capítulos. Por lo que, a falta de validarlo 
experimentalmente. Unos 10-15 minutos

Se llama con la flag --ivoox

python3 ivoox-rss-unlocker.py --rss https://www.ivoox.com/feed_fg_f11409320_filtro_1.xml -o <podcast>.rss --ivoox

## Tengo un servidor sin usar, quiero que no me de problemas

EXPERIMENTAL/POR FINALIZAR

La flag -s descarga los archivos de audio completos y 
los guarda en una carpeta indicada por el usuario.
Modifica la url por la indicada por el usuario.
Esto permite automatizar autoalojar los audios

python3 ivoox-rss-unlocker.py --rss https://www.ivoox.com/feed_fg_f11409320_filtro_1.xml -o map.rss -s http://anubita.duckdns.org/rss/map
